package com.earlynetworks.sceneformtest.depth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentOnAttachListener;

import android.animation.ObjectAnimator;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.earlynetworks.sceneformtest.MainActivity;
import com.earlynetworks.sceneformtest.R;
import com.google.ar.core.Anchor;
import com.google.ar.core.Config;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Session;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Sceneform;
import com.google.ar.sceneform.animation.ModelAnimation;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.CameraStream;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.rendering.RenderableInstance;
import com.google.ar.sceneform.rendering.Texture;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.BaseArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class DepthActivity extends AppCompatActivity implements
        FragmentOnAttachListener, BaseArFragment.OnTapArPlaneListener, BaseArFragment.OnSessionConfigurationListener, ArFragment.OnViewCreatedListener{

    private static final String MODEL_PANDA = "panda.glb";
    private static final String MODEL_TIGER = "tiger.glb";

    private ArFragment arFragment;
    private HashMap<String, Renderable> modelRenderableMap = new HashMap<>();

    private String currentModel = MODEL_PANDA;

    private Texture texture;

    boolean isObjectTextured = false;

    ImageView ivTexture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_depth);
        getSupportFragmentManager().addFragmentOnAttachListener(this);

        if (savedInstanceState == null) {
            if (Sceneform.isSupported(this)) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.arFragment, ArFragment.class, null)
                        .commit();
            }
        }

        LinearLayout llTexture = findViewById(R.id.ll_texture);
        ivTexture = findViewById(R.id.iv_texture);

        llTexture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isObjectTextured){
                    isObjectTextured = false;
                    ivTexture.setImageResource(R.drawable.ic_baseline_check_box_outline_blank_24);
                }else{
                    isObjectTextured = true;
                    ivTexture.setImageResource(R.drawable.ic_baseline_check_box_24);
                }
            }
        });

        loadModels(MODEL_PANDA);
        loadModels(MODEL_TIGER);
        loadTexture();
    }

    public void loadModels(String modelFileName) {
        WeakReference<DepthActivity> weakActivity = new WeakReference<>(this);
        ModelRenderable.builder()
                .setSource(this, Uri.parse(modelFileName))
                .setIsFilamentGltf(true)
                .setAsyncLoadEnabled(true)
                .build()
                .thenAccept(model -> {
                    DepthActivity activity = weakActivity.get();
                    if (activity != null) {
                        modelRenderableMap.put(modelFileName, model);
                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(this, "Unable to load model", Toast.LENGTH_LONG).show();
                    return null;
                });
    }

    public void loadTexture() {
        WeakReference<DepthActivity> weakActivity = new WeakReference<>(this);
        Texture.builder()
                .setSampler(Texture.Sampler.builder()
                        .setMinFilter(Texture.Sampler.MinFilter.LINEAR_MIPMAP_LINEAR)
                        .setMagFilter(Texture.Sampler.MagFilter.LINEAR)
                        .setWrapMode(Texture.Sampler.WrapMode.REPEAT)
                        .build())
                .setSource(this, Uri.parse("parquet.jpg"))
                .setUsage(Texture.Usage.COLOR_MAP)
                .build()
                .thenAccept(
                        texture -> {
                            DepthActivity activity = weakActivity.get();
                            if (activity != null) {
                                activity.texture = texture;
                            }
                        })
                .exceptionally(
                        throwable -> {
                            Toast.makeText(this, "Unable to load texture", Toast.LENGTH_LONG).show();
                            return null;
                        });
    }

    @Override
    public void onAttachFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment) {
        if (fragment.getId() == R.id.arFragment) {
            arFragment = (ArFragment) fragment;
            arFragment.setOnTapArPlaneListener(this);
            arFragment.setOnViewCreatedListener(this);
            arFragment.setOnSessionConfigurationListener(this);
        }
    }

    @Override
    public void onViewCreated(ArFragment arFragment, ArSceneView arSceneView) {
        arFragment.setOnViewCreatedListener(null);

        // Available modes: DEPTH_OCCLUSION_DISABLED, DEPTH_OCCLUSION_ENABLED
        arSceneView.getCameraStream()
                .setDepthOcclusionMode(CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_ENABLED);
    }

    @Override
    public void onTapPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {

        Renderable currentModelRenderable = modelRenderableMap.get(currentModel);

        if (currentModelRenderable == null) {
            Toast.makeText(this, "Loading...", Toast.LENGTH_SHORT).show();
            return;
        }

        // Create the Anchor.
        Anchor anchor = hitResult.createAnchor();
        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setParent(arFragment.getArSceneView().getScene());

        // Create the transformable model and add it to the anchor.
        TransformableNode model = new TransformableNode(arFragment.getTransformationSystem());
        //model.setLocalScale(new Vector3(0.01f, 0.01f, 0.01f));
        model.getScaleController().setMaxScale(0.7f);
        model.getScaleController().setMinScale(0.5f);
        model.setParent(anchorNode);

        RenderableInstance modelInstance =model.setRenderable(currentModelRenderable);

        if(isObjectTextured){
            modelInstance.getMaterial().setInt("baseColorIndex", 0);
            modelInstance.getMaterial().setTexture("baseColorMap", texture);
        }

        ObjectAnimator objectAnimator = modelInstance.animate(true);
        model.select();

        model.setOnTapListener(new Node.OnTapListener() {
            @Override
            public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {

                if(!objectAnimator.isStarted()){
                    objectAnimator.start();
                    return;
                }

                if(objectAnimator.isPaused()){
                    objectAnimator.resume();
                }else{
                    objectAnimator.pause();
                }
            }
        });
    }

    @Override
    public void onSessionConfiguration(Session session, Config config) {
        if (session.isDepthModeSupported(Config.DepthMode.AUTOMATIC)) {
            config.setDepthMode(Config.DepthMode.AUTOMATIC);
        }

        /*if (session.isDepthModeSupported(Config.DepthMode.RAW_DEPTH_ONLY)) {
            config.setDepthMode(Config.DepthMode.RAW_DEPTH_ONLY);
        }*/

        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);
    }


    public void selectObject(View view) {
        switch (view.getId()){
            case R.id.bt_panda:
                currentModel = MODEL_PANDA;
                Toast.makeText(getApplicationContext(), "SELECT MODEL - PANADA", Toast.LENGTH_SHORT).show();
                break;

            case R.id.bt_tiger:
                currentModel = MODEL_TIGER;
                Toast.makeText(getApplicationContext(), "SELECT MODEL - TIGER", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}