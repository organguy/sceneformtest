package com.earlynetworks.sceneformtest.meta_bus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentOnAttachListener;

import android.animation.ObjectAnimator;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.earlynetworks.sceneformtest.R;
import com.earlynetworks.sceneformtest.data.AnchorData;
import com.earlynetworks.sceneformtest.data.ModelData;
import com.earlynetworks.sceneformtest.data.QuarternionData;
import com.earlynetworks.sceneformtest.data.VectorData;
import com.earlynetworks.sceneformtest.depth.DepthActivity;
import com.earlynetworks.sceneformtest.helper.FirebaseDBManager;
import com.google.ar.core.Anchor;
import com.google.ar.core.Config;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Session;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.Sceneform;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.CameraStream;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.Material;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.rendering.RenderableInstance;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.rendering.Texture;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.BaseArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Consumer;

public class MetabusActivity extends AppCompatActivity implements FragmentOnAttachListener,
        BaseArFragment.OnSessionConfigurationListener,
        ArFragment.OnViewCreatedListener,
        Scene.OnUpdateListener{

    private static final String MODEL_PANDA = "panda.glb";
    private static final String MODEL_TIGER = "tiger.glb";

    private ArFragment arFragment;

    AnchorData mAnchorData;

    ModelRenderable placeRenderable;

    TransformableNode placeModel;

    private String currentModel = MODEL_PANDA;


    private HashMap<String, Renderable> modelRenderableMap = new HashMap<>();
    HashMap<String, ModelData> addedModelMap = new HashMap<>();

    boolean isModelHosting = false;

    private Texture texture;
    boolean isObjectTextured = false;
    ImageView ivTexture;
    ImageView ivDepth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metabus);
        getSupportFragmentManager().addFragmentOnAttachListener(this);

        if (savedInstanceState == null) {
            if (Sceneform.isSupported(this)) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.arFragment, ArFragment.class, null)
                        .commit();
            }
        }

        LinearLayout llTexture = findViewById(R.id.ll_texture);
        ivTexture = findViewById(R.id.iv_texture);

        llTexture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isObjectTextured){
                    isObjectTextured = false;
                    ivTexture.setImageResource(R.drawable.ic_baseline_check_box_outline_blank_24);
                }else{
                    isObjectTextured = true;
                    ivTexture.setImageResource(R.drawable.ic_baseline_check_box_24);
                }
            }
        });

        LinearLayout llDepth = findViewById(R.id.ll_depth);
        ivDepth = findViewById(R.id.iv_depth);

        llDepth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CameraStream.DepthOcclusionMode occlusionMode = arFragment.getArSceneView().getCameraStream().getDepthOcclusionMode();

                if(occlusionMode == CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_ENABLED){
                    arFragment.getArSceneView().getCameraStream()
                            .setDepthOcclusionMode(CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_DISABLED);
                    ivDepth.setImageResource(R.drawable.ic_baseline_check_box_outline_blank_24);
                }else{
                    arFragment.getArSceneView().getCameraStream()
                            .setDepthOcclusionMode(CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_ENABLED);
                    ivDepth.setImageResource(R.drawable.ic_baseline_check_box_24);
                }

            }
        });

        llDepth.setVisibility(View.GONE);

        makeVirtualPlane();
        loadModels(MODEL_PANDA);
        loadModels(MODEL_TIGER);
        loadTexture();

        initModelDB();
    }

    private void initModelDB(){
        FirebaseDBManager.getInstance(this).deleteModelAll(new FirebaseDBManager.OnFirebaseResultListener<Void>() {
            @Override
            public void onResult(Void result) {
                Toast.makeText(getApplicationContext(), "DELETE MODELS ALL", Toast.LENGTH_SHORT).show();
                setOnModelAddedListener();
            }
        });
    }

    private void setOnModelAddedListener(){
        FirebaseDBManager.getInstance(this).changeModelDataAdded(new FirebaseDBManager.OnFirebaseResultListener<ModelData>() {
            @Override
            public void onResult(ModelData result) {

                if(!isModelHosting){
                    Toast.makeText(getApplicationContext(), "MODEL CHILD ADDED : " + result.getIndex(), Toast.LENGTH_SHORT).show();
                    addModel(result);
                }else{
                    Toast.makeText(getApplicationContext(), "IS MODEL HOSTING: " + result.getIndex(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void makeVirtualPlane(){
        MaterialFactory
                .makeTransparentWithColor(this, new Color(0.0f, 0.0f, 1.0f, 0.7f))
                .thenAccept(new Consumer<Material>() {
                    @Override
                    public void accept(Material material) {
                        placeRenderable =
                                ShapeFactory.makeCube(
                                        new Vector3(12.0f, 0.01f, 12.0f),
                                        new Vector3(0f, 0.1f, 0f),
                                        material);

                        loadCloudAnchor();
                    }
                });
    }

    private void loadCloudAnchor(){
        FirebaseDBManager.getInstance(this).getAnchor(new FirebaseDBManager.OnFirebaseResultListener<AnchorData>() {
            @Override
            public void onResult(AnchorData anchor) {
                mAnchorData = anchor;

                Anchor resolveAnchor = arFragment.getArSceneView().getSession().resolveCloudAnchor(mAnchorData.getAnchorId());
                placeVirtualPlane(resolveAnchor);

                Toast.makeText(getApplicationContext(), anchor.getAnchorId(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void placeVirtualPlane(Anchor anchor) {

        // Create the Anchor.
        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setParent(arFragment.getArSceneView().getScene());

        // Create the transformable model and add it to the anchor.
        placeModel = new TransformableNode(arFragment.getTransformationSystem());
        //placeModel.setLocalScale(new Vector3(0.01f, 0.01f, 0.01f));

        placeModel.getScaleController().setMaxScale(1.0f);
        placeModel.getScaleController().setMinScale(0.9f);

        placeModel.setParent(anchorNode);
        placeModel.setRenderable(placeRenderable);

        placeModel.setOnTapListener(new Node.OnTapListener() {
            @Override
            public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {


                isModelHosting = true;

                Log.d("HitTestResult", "World Position : " +
                        "x : " + hitTestResult.getNode().getWorldPosition().x + " , " +
                        "y : " + hitTestResult.getNode().getWorldPosition().y + " , " +
                        "z : " + hitTestResult.getNode().getWorldPosition().z
                );

                Log.d("HitTestResult", "Local Position : " +
                        "x : " + hitTestResult.getNode().getLocalPosition().x + " , " +
                        "y : " + hitTestResult.getNode().getLocalPosition().y + " , " +
                        "z : " + hitTestResult.getNode().getLocalPosition().z
                );

                Log.d("HitTestResult", "MotionEvent : " +
                        "x : " + motionEvent.getX() + " , " +
                        "y : " + motionEvent.getY()
                );


                hitModel(hitTestResult);
            }
        });
    }

    public void addModel(ModelData modelData){

        String modelType = MODEL_PANDA;

        if(modelData.getModelType() == ModelData.ModelType.PANADA){
            modelType = MODEL_PANDA;
        }else if(modelData.getModelType() == ModelData.ModelType.TIGER){
            modelType = MODEL_TIGER;
        }

        Renderable modelRenderable = modelRenderableMap.get(modelType);

        TransformableNode model = new TransformableNode(arFragment.getTransformationSystem());
        model.getScaleController().setMaxScale(0.4f);
        model.getScaleController().setMinScale(0.3999999f);
        model.setParent(placeModel);

        Vector3 position = new Vector3(modelData.getPosition().getX(), modelData.getPosition().getY(), modelData.getPosition().getZ());
        Vector3 scale = new Vector3(modelData.getScale().getX(), modelData.getScale().getY(), modelData.getScale().getZ());
        Quaternion rotation = Quaternion.axisAngle(new Vector3(modelData.getRotation().getX(), modelData.getRotation().getY(), modelData.getRotation().getZ()), modelData.getRotation().getW());

        model.setWorldPosition(position);
        model.setWorldScale(scale);
        model.setWorldRotation(rotation);

        RenderableInstance modelInstance = model.setRenderable(modelRenderable);

        if(modelData.getTextureType() == ModelData.TextureType.WOOD){
            modelInstance.getMaterial().setInt("baseColorIndex", 0);
            modelInstance.getMaterial().setTexture("baseColorMap", texture);
        }

        ObjectAnimator objectAnimator = modelInstance.animate(true);
        model.select();

        model.setOnTapListener(new Node.OnTapListener() {
            @Override
            public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {

                if(!objectAnimator.isStarted()){
                    objectAnimator.start();
                    return;
                }

                if(objectAnimator.isPaused()){
                    objectAnimator.resume();
                }else{
                    objectAnimator.pause();
                }
            }
        });
    }


    public void hitModel(HitTestResult hitTestResult){

        Renderable currentModelRenderable = modelRenderableMap.get(currentModel);

        if (currentModelRenderable == null) {
            Toast.makeText(this, "Loading...", Toast.LENGTH_SHORT).show();
            return;
        }


        // Create the transformable model and add it to the anchor.
        TransformableNode model = new TransformableNode(arFragment.getTransformationSystem());
        //model.setLocalScale(new Vector3(0.01f, 0.01f, 0.01f));
        model.getScaleController().setMaxScale(0.4f);
        model.getScaleController().setMinScale(0.3999999f);
        model.setParent(hitTestResult.getNode());
        model.setWorldPosition(hitTestResult.getPoint());

        RenderableInstance modelInstance = model.setRenderable(currentModelRenderable);

        if(isObjectTextured){
            modelInstance.getMaterial().setInt("baseColorIndex", 0);
            modelInstance.getMaterial().setTexture("baseColorMap", texture);
        }

        ObjectAnimator objectAnimator = modelInstance.animate(true);
        model.select();

        model.setOnTapListener(new Node.OnTapListener() {
            @Override
            public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {

                if(!objectAnimator.isStarted()){
                    objectAnimator.start();
                    return;
                }

                if(objectAnimator.isPaused()){
                    objectAnimator.resume();
                }else{
                    objectAnimator.pause();
                }
            }
        });

        insertModel(model);
    }

    public void loadModels(String modelFileName) {
        WeakReference<MetabusActivity> weakActivity = new WeakReference<>(this);
        ModelRenderable.builder()
                .setSource(this, Uri.parse(modelFileName))
                .setIsFilamentGltf(true)
                .setAsyncLoadEnabled(true)
                .build()
                .thenAccept(model -> {
                    MetabusActivity activity = weakActivity.get();
                    if (activity != null) {
                        modelRenderableMap.put(modelFileName, model);
                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(this, "Unable to load model", Toast.LENGTH_LONG).show();
                    return null;
                });
    }

    public void loadTexture() {
        WeakReference<MetabusActivity> weakActivity = new WeakReference<>(this);
        Texture.builder()
                .setSampler(Texture.Sampler.builder()
                        .setMinFilter(Texture.Sampler.MinFilter.LINEAR_MIPMAP_LINEAR)
                        .setMagFilter(Texture.Sampler.MagFilter.LINEAR)
                        .setWrapMode(Texture.Sampler.WrapMode.REPEAT)
                        .build())
                .setSource(this, Uri.parse("parquet.jpg"))
                .setUsage(Texture.Usage.COLOR_MAP)
                .build()
                .thenAccept(
                        texture -> {
                            MetabusActivity activity = weakActivity.get();
                            if (activity != null) {
                                activity.texture = texture;
                            }
                        })
                .exceptionally(
                        throwable -> {
                            Toast.makeText(this, "Unable to load texture", Toast.LENGTH_LONG).show();
                            return null;
                        });
    }

    private void insertModel(Node model){
        ModelData modelData = new ModelData();

        if(currentModel.equals(MODEL_PANDA)){
            modelData.setModelType(ModelData.ModelType.PANADA);
        }else{
            modelData.setModelType(ModelData.ModelType.TIGER);
        }

        if(!isObjectTextured){
            modelData.setTextureType(ModelData.TextureType.DEFAULT);
        }else{
            modelData.setTextureType(ModelData.TextureType.WOOD);
        }

        modelData.setTransformable(false);
        modelData.setCreateDate(new Date());
        modelData.setUpdateDate(new Date());

        VectorData position = new VectorData();
        modelData.setPosition(position);

        position.setX(model.getWorldPosition().x);
        position.setY(model.getWorldPosition().y);
        position.setZ(model.getWorldPosition().z);

        VectorData scale = new VectorData();
        modelData.setScale(scale);

        scale.setX(model.getWorldScale().x);
        scale.setY(model.getWorldScale().y);
        scale.setZ(model.getWorldScale().z);

        QuarternionData rotation = new QuarternionData();
        modelData.setRotation(rotation);

        rotation.setX(model.getWorldRotation().x);
        rotation.setY(model.getWorldRotation().y);
        rotation.setZ(model.getWorldRotation().z);
        rotation.setW(model.getWorldRotation().w);

        FirebaseDBManager.getInstance(this).insertModel(modelData, new FirebaseDBManager.OnFirebaseResultListener<ModelData>() {
            @Override
            public void onResult(ModelData result) {

                Toast.makeText(getApplicationContext(), "Model Insert Success !!! : " + result.getIndex(), Toast.LENGTH_SHORT).show();

                isModelHosting = false;
            }
        });
    }

    @Override
    public void onAttachFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment) {
        if (fragment.getId() == R.id.arFragment) {
            arFragment = (ArFragment) fragment;
            arFragment.setOnViewCreatedListener(this);
            arFragment.setOnSessionConfigurationListener(this);
        }
    }

    @Override
    public void onViewCreated(ArFragment arFragment, ArSceneView arSceneView) {
        arFragment.setOnViewCreatedListener(null);

        // Available modes: DEPTH_OCCLUSION_DISABLED, DEPTH_OCCLUSION_ENABLED
        arSceneView.getCameraStream()
                .setDepthOcclusionMode(CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_ENABLED);

        arSceneView.getScene().addOnUpdateListener(this);
    }

    @Override
    public void onUpdate(FrameTime frameTime) {

    }

    @Override
    public void onSessionConfiguration(Session session, Config config) {
        if (session.isDepthModeSupported(Config.DepthMode.AUTOMATIC)) {
            config.setDepthMode(Config.DepthMode.AUTOMATIC);
        }

        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);

        config.setCloudAnchorMode(Config.CloudAnchorMode.ENABLED);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_panda:
                currentModel = MODEL_PANDA;
                Toast.makeText(getApplicationContext(), "SELECT MODEL - PANADA", Toast.LENGTH_SHORT).show();
                break;

            case R.id.bt_tiger:
                currentModel = MODEL_TIGER;
                Toast.makeText(getApplicationContext(), "SELECT MODEL - TIGER", Toast.LENGTH_SHORT).show();
                break;

            case R.id.bt_back:
                finish();
                break;
        }
    }
}