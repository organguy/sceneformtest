package com.earlynetworks.sceneformtest.helper;

import android.content.Context;
import android.widget.Toast;

import com.earlynetworks.sceneformtest.data.AnchorData;
import com.earlynetworks.sceneformtest.data.ModelData;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.flatbuffers.Table;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FirebaseDBManager {

    Context mCtx;
    DatabaseReference rootRef;
    DatabaseReference anchorTable;
    DatabaseReference modelTable;


    public interface OnFirebaseResultListener<T> {
        void onResult(T result);
    }

    public static FirebaseDBManager instance;

    public static FirebaseDBManager getInstance(Context ctx) {
        if (instance == null) {
            instance = new FirebaseDBManager(ctx);
        }
        return instance;
    }

    public FirebaseDBManager(Context ctx) {
        this.mCtx = ctx;
        this.rootRef = FirebaseDatabase.getInstance().getReference();
        this.anchorTable = rootRef.child(DBConstant.AnchorTable.TABLE_NAME);
        this.modelTable = rootRef.child(DBConstant.ModelTable.TABLE_NAME);
    }

    public void isAnchorExist(OnFirebaseResultListener<Boolean> listener){

        anchorTable.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        if(snapshot.exists() && snapshot.getChildrenCount() > 0){
                            AnchorData anchorData = snapshot.getValue(AnchorData.class);
                            listener.onResult(true);
                        }else{
                            listener.onResult(false);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(mCtx, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void getAnchor(OnFirebaseResultListener<AnchorData> listener){

        anchorTable
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){

                            AnchorData anchorData;

                            for (DataSnapshot child : snapshot.getChildren()){
                                anchorData = child.getValue(AnchorData.class);
                                listener.onResult(anchorData);
                                break;
                            }
                        }else{
                            listener.onResult(null);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(mCtx, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }



    public void insertAnchor(AnchorData anchorData, OnFirebaseResultListener<AnchorData> listener){

        String index = anchorTable.push().getKey();
        anchorData.setIndex(index);

        anchorTable.child(index)
                .setValue(anchorData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        listener.onResult(anchorData);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        listener.onResult(null);
                        Toast.makeText(mCtx, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        listener.onResult(null);
                    }
                });

    }



    public void updateAnchor(AnchorData anchorData, OnFirebaseResultListener<Boolean> listener){

        anchorTable.child(anchorData.getIndex())
                .setValue(anchorData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        listener.onResult(true);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        listener.onResult(false);
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        listener.onResult(false);
                    }
                });

    }

    public void insertModel(ModelData modelData, OnFirebaseResultListener<ModelData> listener){

        String index = modelTable.push().getKey();
        modelData.setIndex(index);

        modelTable.child(index)
                .setValue(modelData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        listener.onResult(modelData);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        listener.onResult(null);
                        Toast.makeText(mCtx, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        listener.onResult(null);
                    }
                });

    }

    public void deleteModelAll(OnFirebaseResultListener<Void> listener){
        modelTable.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                listener.onResult(unused);
            }
        });
    }

    public void changeModelDataAdded(OnFirebaseResultListener<ModelData> listener){

        modelTable.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                if(snapshot.exists()){

                    ModelData modelData = snapshot.getValue(ModelData.class);
                    listener.onResult(modelData);

                }else{
                    listener.onResult(null);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
