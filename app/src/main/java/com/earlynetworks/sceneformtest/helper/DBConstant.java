package com.earlynetworks.sceneformtest.helper;

import android.provider.BaseColumns;

/**
 * DB 테이블 정의
 */
public class DBConstant {
	/**
	 * 회원 테이블
	 */
	public static class AnchorTable implements BaseColumns {
		public static final String TABLE_NAME = "Anchor";
		public static final String ANCHOR_INDEX = "user_index";
		public static final String ANCHOR_ID = "user_id";
	}

	public static class ModelTable implements BaseColumns {
		public static final String TABLE_NAME = "Model";
		public static final String ANCHOR_INDEX = "user_index";
		public static final String ANCHOR_ID = "user_id";
	}
}
