package com.earlynetworks.sceneformtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.earlynetworks.sceneformtest.cloud_anchor.CloudAnchorActivity;
import com.earlynetworks.sceneformtest.depth.DepthActivity;
import com.earlynetworks.sceneformtest.meta_bus.MetabusActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_depth:
                moveSelectPage(DepthActivity.class);
                break;

            case R.id.bt_anchor:
                moveSelectPage(CloudAnchorActivity.class);
                break;

            case R.id.bt_metabus:
                moveSelectPage(MetabusActivity.class);
                break;
        }
    }

    private void moveSelectPage(Class className){
        Intent intent = new Intent(this, className);
        startActivity(intent);
    }
}