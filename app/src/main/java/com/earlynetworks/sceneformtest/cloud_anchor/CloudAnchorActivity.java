package com.earlynetworks.sceneformtest.cloud_anchor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentOnAttachListener;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.earlynetworks.sceneformtest.R;
import com.earlynetworks.sceneformtest.data.AnchorData;
import com.earlynetworks.sceneformtest.data.ModelData;
import com.earlynetworks.sceneformtest.data.QuarternionData;
import com.earlynetworks.sceneformtest.data.VectorData;
import com.earlynetworks.sceneformtest.helper.FirebaseDBManager;
import com.google.ar.core.Anchor;
import com.google.ar.core.Config;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Session;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.HitTestResult;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.Sceneform;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.CameraStream;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.Material;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.BaseArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Consumer;

public class CloudAnchorActivity extends AppCompatActivity implements FragmentOnAttachListener,
        BaseArFragment.OnTapArPlaneListener,
        BaseArFragment.OnSessionConfigurationListener,
        ArFragment.OnViewCreatedListener,
        Scene.OnUpdateListener
{

    private ArFragment arFragment;
    private HashMap<String, Renderable> modelRenderableMap = new HashMap<>();

    private enum AppAnchorState{
        NONE,
        HOSTING,
        HOSTED
    }

    private Anchor mAnchor;
    private Anchor mCloudAnchor;
    private AppAnchorState appAnchorState = AppAnchorState.NONE;
    private boolean isPlaced = false;

    ModelRenderable placeRenderable;

    AnchorData mAnchorData;

    TransformableNode placeModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_anchor);
        getSupportFragmentManager().addFragmentOnAttachListener(this);

        if (savedInstanceState == null) {
            if (Sceneform.isSupported(this)) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.arFragment, ArFragment.class, null)
                        .commit();
            }
        }

        FirebaseDBManager.getInstance(this).getAnchor(result -> {
            mAnchorData = result;
        });

        makeVirtualPlane();
    }

    private void makeVirtualPlane(){
        MaterialFactory
                .makeOpaqueWithColor(this, new Color(android.graphics.Color.BLUE))
                .thenAccept(new Consumer<Material>() {
                    @Override
                    public void accept(Material material) {
                        placeRenderable =
                                ShapeFactory.makeCube(
                                        new Vector3(12.0f, 0.01f, 12.0f),
                                        new Vector3(0f, 0.1f, 0f),
                                        material);
                    }
                });
    }

    @Override
    public void onAttachFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment) {
        if (fragment.getId() == R.id.arFragment) {
            arFragment = (ArFragment) fragment;
            arFragment.setOnTapArPlaneListener(this);
            arFragment.setOnViewCreatedListener(this);
            arFragment.setOnSessionConfigurationListener(this);
        }
    }

    @Override
    public void onViewCreated(ArFragment arFragment, ArSceneView arSceneView) {
        arFragment.setOnViewCreatedListener(null);

        // Available modes: DEPTH_OCCLUSION_DISABLED, DEPTH_OCCLUSION_ENABLED
        arSceneView.getCameraStream()
                .setDepthOcclusionMode(CameraStream.DepthOcclusionMode.DEPTH_OCCLUSION_DISABLED);

        arSceneView.getScene().addOnUpdateListener(this);
    }

    @Override
    public void onTapPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {

        if(!isPlaced) {

            isPlaced = true;

            mAnchor = hitResult.createAnchor();

            placeVirtualPlane(mAnchor);
        }

    }

    private void placeVirtualPlane(Anchor anchor) {

        // Create the Anchor.
        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setParent(arFragment.getArSceneView().getScene());

        // Create the transformable model and add it to the anchor.
        placeModel = new TransformableNode(arFragment.getTransformationSystem());
        //placeModel.setLocalScale(new Vector3(0.01f, 0.01f, 0.01f));

        placeModel.getScaleController().setMaxScale(1.0f);
        placeModel.getScaleController().setMinScale(0.9f);

        placeModel.setParent(anchorNode);
        placeModel.setRenderable(placeRenderable);

        placeModel.setOnTapListener(new Node.OnTapListener() {
            @Override
            public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {
                Log.d("HitTestResult", "World Position : " +
                        "x : " + hitTestResult.getNode().getWorldPosition().x + " , " +
                        "y : " + hitTestResult.getNode().getWorldPosition().y + " , " +
                        "z : " + hitTestResult.getNode().getWorldPosition().z
                );

                Log.d("HitTestResult", "Local Position : " +
                        "x : " + hitTestResult.getNode().getLocalPosition().x + " , " +
                        "y : " + hitTestResult.getNode().getLocalPosition().y + " , " +
                        "z : " + hitTestResult.getNode().getLocalPosition().z
                );

                Log.d("HitTestResult", "MotionEvent : " +
                        "x : " + motionEvent.getX() + " , " +
                        "y : " + motionEvent.getY()
                );


            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_save:
                saveCloudAnchor();
                break;

            case R.id.bt_load:
                loadCloudAnchor();
                break;

            case R.id.bt_back:
                finish();
                break;
        }
    }

    private void saveCloudAnchor(){
        mCloudAnchor = arFragment.getArSceneView().getSession().hostCloudAnchor(mAnchor);
        appAnchorState = AppAnchorState.HOSTING;
        Toast.makeText(getApplicationContext(), "Hosting...", Toast.LENGTH_SHORT).show();

    }

    private void loadCloudAnchor(){
        FirebaseDBManager.getInstance(this).getAnchor(new FirebaseDBManager.OnFirebaseResultListener<AnchorData>() {
            @Override
            public void onResult(AnchorData anchor) {
                mAnchorData = anchor;

                Anchor resolveAnchor = arFragment.getArSceneView().getSession().resolveCloudAnchor(mAnchorData.getAnchorId());
                placeVirtualPlane(resolveAnchor);

                Toast.makeText(getApplicationContext(), anchor.getAnchorId(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onUpdate(FrameTime frameTime) {
        if(appAnchorState != AppAnchorState.HOSTING)
            return;

        Anchor.CloudAnchorState cloudAnchorState = mCloudAnchor.getCloudAnchorState();

        if(cloudAnchorState.isError()){
            Toast.makeText(getApplicationContext(), cloudAnchorState.toString(), Toast.LENGTH_SHORT).show();
        }else if(cloudAnchorState == Anchor.CloudAnchorState.SUCCESS){
            appAnchorState = AppAnchorState.HOSTED;

            String anchorId = mCloudAnchor.getCloudAnchorId();

            updateAnchorData(anchorId);

        }
    }

    private void updateAnchorData(String anchorId){
        AnchorData anchorData = new AnchorData();
        anchorData.setAnchorId(anchorId);

        ModelData modelData = new ModelData();
        anchorData.setPlane(modelData);

        modelData.setModelType(ModelData.ModelType.PLANE);
        modelData.setTransformable(false);
        modelData.setUpdateDate(new Date());

        VectorData position = new VectorData();
        modelData.setPosition(position);

        position.setX(placeModel.getLocalPosition().x);
        position.setY(placeModel.getLocalPosition().y);
        position.setZ(placeModel.getLocalPosition().z);

        VectorData scale = new VectorData();
        modelData.setScale(scale);

        scale.setX(placeModel.getLocalScale().x);
        scale.setY(placeModel.getLocalScale().y);
        scale.setZ(placeModel.getLocalScale().z);

        QuarternionData rotation = new QuarternionData();
        modelData.setRotation(rotation);

        rotation.setX(placeModel.getLocalRotation().x);
        rotation.setY(placeModel.getLocalRotation().y);
        rotation.setZ(placeModel.getLocalRotation().z);
        rotation.setW(placeModel.getLocalRotation().w);

        if(mAnchorData != null){

            anchorData.setIndex(mAnchorData.getIndex());

            FirebaseDBManager.getInstance(this).updateAnchor(anchorData, new FirebaseDBManager.OnFirebaseResultListener<Boolean>() {
                @Override
                public void onResult(Boolean result) {
                    Toast.makeText(getApplicationContext(), "Anchor hosted successfully. Anchor ID : " + anchorId, Toast.LENGTH_SHORT).show();
                }
            });
        }else{

            modelData.setCreateDate(new Date());

            FirebaseDBManager.getInstance(this).insertAnchor(anchorData, new FirebaseDBManager.OnFirebaseResultListener<AnchorData>() {
                @Override
                public void onResult(AnchorData result) {
                    mAnchorData = result;
                    Toast.makeText(getApplicationContext(), "Anchor hosted successfully. Anchor ID : " + anchorId, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public void onSessionConfiguration(Session session, Config config) {
        if (session.isDepthModeSupported(Config.DepthMode.AUTOMATIC)) {
            config.setDepthMode(Config.DepthMode.AUTOMATIC);
        }

        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);

        config.setCloudAnchorMode(Config.CloudAnchorMode.ENABLED);
    }

}