package com.earlynetworks.sceneformtest.data;

public class AnchorData {
    private String index;
    private String anchorId;
    private ModelData plane;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getAnchorId() {
        return anchorId;
    }

    public void setAnchorId(String anchorId) {
        this.anchorId = anchorId;
    }

    public ModelData getPlane() {
        return plane;
    }

    public void setPlane(ModelData plane) {
        this.plane = plane;
    }
}
