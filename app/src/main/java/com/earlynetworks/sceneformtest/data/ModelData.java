package com.earlynetworks.sceneformtest.data;

import java.util.Date;

public class ModelData {

    public enum ModelType{
        PLANE, PANADA, TIGER
    }

    public enum TextureType{
        DEFAULT, WOOD
    }

    String index;
    ModelType modelType;
    TextureType textureType;
    boolean isTransformable = false;
    private VectorData position;
    private VectorData scale;
    private QuarternionData rotation;
    private Date createDate;
    private Date updateDate;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public ModelType getModelType() {
        return modelType;
    }

    public void setModelType(ModelType modelType) {
        this.modelType = modelType;
    }

    public TextureType getTextureType() {
        return textureType;
    }

    public void setTextureType(TextureType textureType) {
        this.textureType = textureType;
    }

    public boolean isTransformable() {
        return isTransformable;
    }

    public void setTransformable(boolean transformable) {
        isTransformable = transformable;
    }

    public VectorData getPosition() {
        return position;
    }

    public void setPosition(VectorData position) {
        this.position = position;
    }

    public VectorData getScale() {
        return scale;
    }

    public void setScale(VectorData scale) {
        this.scale = scale;
    }

    public QuarternionData getRotation() {
        return rotation;
    }

    public void setRotation(QuarternionData rotation) {
        this.rotation = rotation;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
